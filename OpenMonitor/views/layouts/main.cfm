<cfoutput>
<!DOCTYPE html>
<head>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
		<meta name="format-detection" content="telephone=no">
		<meta charset="UTF-8">

		<!-- Javascript Libraries -->
		#javaScriptIncludeTag("
			jquery-3.2.1.min,
			bootstrap.min,
			jquery.flot.min,
			jquery.flot.resize.min,
			jquery.flot.time.min
		")#
		#styleSheetLinkTag("
			bootstrap.min,
			font-awesome.min,
			thermometre,
			openmonitor
		")#

</head>
<body>
	<nav class="navbar navbar-default" >
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="##bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="##">Openmonitor</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>

<section id="content">

			#includeContent()#

</section>
</body>
</cfoutput>