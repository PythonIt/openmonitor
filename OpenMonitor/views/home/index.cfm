<cfoutput>
<script src='https://ajax.googleapis.com/ajax/libs/angularjs/1.5.8/angular.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.1/js/materialize.min.js'></script>
<script src='https://code.highcharts.com/highcharts.js'></script>
<script src='https://code.highcharts.com/highcharts-more.js'></script>
<script src='https://code.highcharts.com/modules/solid-gauge.js'></script>

<style>
.led-off {
    margin: 20px auto;
    width: 30px;
    height: 30px;
    border-radius: 50%;
    background-color: ##E0E0E0;
    box-shadow: inset ##000 0 -1px 4px 1px;
}

.led-on {
    margin: 20px auto;
    width: 50px;
    height: 50px;
    border-radius: 50%;
    background-color: ##80FF00;
    box-shadow: inset ##000 0 -1px 4px 1px;
}
</style>

<div class="container" style="background-color:white;width:85%;">

	<div class="row">
		<div class="col-md-2"></div><div id="laide" class="led led-on col-md-2"></div><div class="col-md-2"></div><div class="h1 col-md-4">Open space dashboard</div>
	</div>

	<div class="tile row">
		<div class="col-md-11">
			<div id="line-chart" style="height:400px;" class="mc-item"></div>
	        <div class="flc-dynamic"></div>
		</div>
		<div class="col-md-1">
			<div class="tg-thermometer" style="height: 400px;">
		    <div class="draw-a"></div>
		    <div class="draw-b"></div>
		    <div class="meter">
		      <div class="statistics">
		        <div class="percent percent-a">50�</div>
		        <div class="percent percent-c">25�</div>
		        <div class="percent percent-e">0�</div>
		      </div>
		      <div class="mercury" id="theight" style="height: 20%">
		        <div class="percent-current" id="temperature"></div>
		        <div class="mask">
		          <div class="bg-color"></div>
		        </div>
		      </div>
		    </div>
		  </div>
		</div>
	<div>
<hr style="height:3px;border:none;color:##e8ecf2;background-color:##e8ecf2;">
	<div class="row">
		<div class="col-lg-3"> <div id="container-speed" style="width: 300px; height: 200px; float: left"></div></div>
		<div class="col-lg-3"> <div id="container-rpm" style="width: 300px; height: 200px; float: left"></div></div>
		<div class="col-lg-3"> <div id="container-pressure" style="width: 300px; height: 200px; float: left"></div></div>
		<div class="col-lg-3"> <div id="container-light" style="width: 300px; height: 200px; float: left"></div></div>
	<div>
</div>



<script type="text/javascript">

$(document).ready(function(){
	 /* Make some random data for Flot Line Chart */
    var #toScript(arrayChart, "data1")#
    window.globalColor = "##009688";
	window.globalVar = data1;
	window.globalAir = #air.sensorvalue#;
	window.globalHumidity = #humidity.sensorvalue#;
	window.globalPressure = #pressure.sensorvalue#;
	window.globalLight = #light.sensorvalue#;
	$("##temperature").val(#temperature.sensorvalue#);
	$("##theight").css({"height": #temperature.sensorvalue * 2#});

    /* Chart Options */
	var totalPoints = 300;
    var updateInterval = 500;

    var options = {
         series: {
            shadowSize: 0,
            curvedLines: {
                apply: true,
                active: true,
                monotonicFit: true,

            },
            lines: {
                show: true,
                lineWidth: 1,
                fill:0.75
            },
             color: window.globalColor,
                shadowSize: 0,

        },
        grid: {

            borderWidth: 0,
            labelMargin:10,
            hoverable: true,
            clickable: true,
            mouseActiveRadius:6,
        },
        xaxis: {
            tickDecimals: 0,
   			show : false,
   			min : 0,
   			max : 59,
   			font :{
                    lineHeight: 13,
                    style: "normal",
                    color: "##9f9f9f",
                },

        },

        yaxis: {
        	 min: 0,
             max: 300,
			tickDecimals: 0,
			tickFormatter: function(val, axis) {
				return val < axis.max ? val.toFixed(0) : "Decibels";
			},
			font :{
                    lineHeight: 13,
                    style: "normal",
                    color: "##9f9f9f",
                }

        },
        legend: {
            show: true
        }
    };

    /* Regular Chart */

     var plot = $.plot($("##line-chart"), [{
	        data : data1,
	        lines: {
	        	show: true,
	        	fill: 0.75
	        },
	        curvedLines: {
	        	apply: true,
	        	active : true
	        },
	        label: 'Decibels',
	        stack: true,
	        color: '##3399ff'
	    }],
	    options
	);



	  /* Update */
    function update() {
    	// This is the URL which the ajax request will fire at; I'm grabbing the HREF of the button as it's more flexible
			var remoteURL = "http://dev.openmonitor.com/home/update-sonority?format=json";
			// The actual AJAX call
			$.ajax({
				url: remoteURL,
				// This function fires when the request is successful: it takes the result of the ajax request and replaces the HTML in the target <div>
				success: function(result){
					console.log(result);
					for (i = 0; i < result.length; i++) {
						result[i][0] = i;
					}

					if (result[59][1] > 100 || result[58][1] > 100 || result[57][1] > 100){
						$("##laide").css({"background-color": "##e22712"});
					}
					else {
						$("##laide").css({"background-color": "##80FF00"});
					}

					window.globalVar = result;
				}
			});
			var remoteURL2 = "http://dev.openmonitor.com/home/update-temperature?format=json";
			$.ajax({
				url: remoteURL2,
				// This function fires when the request is successful: it takes the result of the ajax request and replaces the HTML in the target <div>
				success: function(result2){
					var res = result2 *2;
					res += "%";
					$("##theight").css({"height": res});
					result2 += "�";
					$("##temperature").text(result2);


				}
			});
			var remoteURL3 = "http://dev.openmonitor.com/home/update-air?format=json";
			$.ajax({
				url: remoteURL3,
				// This function fires when the request is successful: it takes the result of the ajax request and replaces the HTML in the target <div>
				success: function(result3){
					window.globalAir = result3;
				}
			});
			var remoteURL4 = "http://dev.openmonitor.com/home/update-humidity?format=json";
			$.ajax({
				url: remoteURL4,
				// This function fires when the request is successful: it takes the result of the ajax request and replaces the HTML in the target <div>
				success: function(result4){
					window.globalHumidity = result4;
				}
			});
			var remoteURL5 = "http://dev.openmonitor.com/home/update-pressure?format=json";
			$.ajax({
				url: remoteURL5,
				// This function fires when the request is successful: it takes the result of the ajax request and replaces the HTML in the target <div>
				success: function(result5){
					window.globalPressure = result5;
				}
			});
			var remoteURL6 = "http://dev.openmonitor.com/home/update-light?format=json";
			$.ajax({
				url: remoteURL6,
				// This function fires when the request is successful: it takes the result of the ajax request and replaces the HTML in the target <div>
				success: function(result6){
					window.globalLight = result6;
				}
			});
        // Since the axes don't change, we don't need to call plot.setupGrid()
	/*	$.plot($("##line-chart"), [{
	        data : window.globalVar,
	        lines: {
	        	show: true,
	        	fill: 0.75
	        },
	        curvedLines: {
	        	apply: true,
	        	active : true
	        },
	        label: 'Decibels',
	        stack: true,
	        color: '##3399ff'
	    }],
	    options
	);*/
		plot.setData([window.globalVar]);
		//plot.setupGrid();
        plot.draw();
        setTimeout(update, updateInterval);
    }
    update();
    ////////////////////////////////////////////////////////////////
    var gaugeOptions = {

    chart: {
        type: 'solidgauge'
    },

    title: null,

    pane: {
        center: ['50%', '85%'],
        size: '140%',
        startAngle: -90,
        endAngle: 90,
        background: {
            backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || '##EEE',
            innerRadius: '60%',
            outerRadius: '100%',
            shape: 'arc'
        }
    },

    tooltip: {
        enabled: false
    },

    // the value axis
    yAxis: {
        stops: [
            [0.1, '##55BF3B'], // green
            [0.5, '##DDDF0D'], // yellow
            [0.9, '##DF5353'] // red
        ],
        lineWidth: 0,
        minorTickInterval: null,
        tickAmount: 2,
        title: {
            y: -70
        },
        labels: {
            y: 16
        }
    },

    plotOptions: {
        solidgauge: {
            dataLabels: {
                y: 5,
                borderWidth: 0,
                useHTML: true
            }
        }
    }
};

// The speed gauge
var chartSpeed = Highcharts.chart('container-speed', Highcharts.merge(gaugeOptions, {
    yAxis: {
        min: 0,
        max: 200,
        title: {
            text: 'Air quality'
        }
    },

    credits: {
        enabled: false
    },

    series: [{
        name: 'Speed',
        data: [window.globalAir],
        dataLabels: {
            format: '<div style="text-align:center"><span style="font-size:25px;color:' +
                ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y}</span><br/>' +
                   '<span style="font-size:12px;color:silver">&zwnj;&zwnj;&zwnj;&zwnj;</span></div>'
        },
        tooltip: {
            valueSuffix: ' km/h'
        }
    }]

}));

// The RPM gauge
var chartRpm = Highcharts.chart('container-rpm', Highcharts.merge(gaugeOptions, {
    yAxis: {
        min: 0,
        max: 100,
        title: {
            text: 'Humidity '
        }
    },

     credits: {
        enabled: false
    },

    series: [{
        name: 'Humidity',
        data: [window.globalHumidity],
        dataLabels: {
            format: '<div style="text-align:center"><span style="font-size:25px;color:' +
                ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y} %</span><br/>' +
                   '<span style="font-size:12px;color:silver">hr</span></div>'
        },
        tooltip: {
            valueSuffix: ' revolutions/min'
        }
    }]

}));

// Pressure
var chartPressure = Highcharts.chart('container-pressure', Highcharts.merge(gaugeOptions, {
    yAxis: {
        min: 900,
        max: 1300,
        title: {
            text: 'Pressure'
        }
    },

     credits: {
        enabled: false
    },

    series: [{
        name: 'Pressure',
        data: [window.globalPressure],
        dataLabels: {
            format: '<div style="text-align:center"><span style="font-size:25px;color:' +
                ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y}</span><br/>' +
                   '<span style="font-size:12px;color:silver">hPa</span></div>'
        },
        tooltip: {
            valueSuffix: ' revolutions/min'
        }
    }]

}));

// Light
var chartLight = Highcharts.chart('container-light', Highcharts.merge(gaugeOptions, {
    yAxis: {
        min: 0,
        max: 1400,
        title: {
            text: 'Light'
        }
    },

     credits: {
        enabled: false
    },

    series: [{
        name: 'Light',
        data: [window.globalLight],
        dataLabels: {
            format: '<div style="text-align:center"><span style="font-size:25px;color:' +
                ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y}</span><br/>' +
                   '<span style="font-size:12px;color:silver">Lumen</span></div>'
        },
        tooltip: {
            valueSuffix: ' revolutions/min'
        }
    }]

}));

// Bring life to the dials
setInterval(function () {
    // Air
    var point,
        newVal,
        inc;

    if (chartSpeed) {
        point = chartSpeed.series[0].points[0];
        point.update(window.globalAir);
    }

    // Humidity
    if (chartRpm) {
        point = chartRpm.series[0].points[0];
        point.update(window.globalHumidity);
    }
     // Pressure
    if (chartPressure) {
        point = chartPressure.series[0].points[0];
        point.update(window.globalPressure);
    }

     // Light
    if (chartLight) {
        point = chartLight.series[0].points[0];
        point.update(window.globalLight);
    }
}, 2000);
});

</script>


</cfoutput>