<cfoutput>

<div class="container">
	<div class="tile" >
		<div class="main-chart" >
			<div id="line-chart" style="height:400px;" class="mc-item" ></div>
		</div>
	<div>
</div>

<script type="text/javascript">


    /* Make some random data for Flot Line Chart */
    var #toScript(arrayChart, "data1")#
    /* Chart Options */

    var options = {
         series: {
            shadowSize: 0,
            curvedLines: {
                apply: true,
                active: true,
                monotonicFit: true,

            },
            lines: {
                show: true,
                lineWidth: 1,
            },
        },
        grid: {

            borderWidth: 0,
            labelMargin:10,
            hoverable: true,
            clickable: true,
            mouseActiveRadius:6,
        },
        xaxis: {
            tickDecimals: 0,
           	minTickSize : [1, "minute"],
           	mode: "time",
   			timeformat: "%H:%M:%S",

        },

        yaxis: {
			tickDecimals: 0,
			min: 0,
			tickFormatter: function(val, axis) {
				return val < axis.max ? val.toFixed(0) : "##";
			}
        },
        legend: {
            show: true
        }
    };

    /* Regular Chart */

    $.plot($("##line-chart"), [{
	        data : data1,
	        lines: {
	        	show: true,
	        	fill: 0.75
	        },
	        curvedLines: {
	        	apply: true,
	        	active : true
	        },
	        label: 'Decibels',
	        stack: true,
	        color: '##3399ff'
	    }],
	    options
	);

	/* Tooltips for Flot Charts */
	$(".mc-item").bind("plothover", function (event, pos, item) {
		if (item.datapoint[1] % 1 == 0) {
			var y = item.datapoint[1].toFixed(0);
			$(".flot-tooltip").html( y + " movements").css({top: item.pageY+5, left: item.pageX+5}).show();
		}
	});
	$("<div class='flot-tooltip chart-tooltip'></div>").appendTo("body");

</script>

</cfoutput>