<cfscript>

	// Use this file to configure specific settings for the "development" environment.
	// A variable set in this file will override the one in "config/settings.cfm".

	// Example:
	// set(dataSourceName="devDB");

	set(dataSourceName="hackathonmic");
	set(dataSourceUserName="root");
	set(dataSourcePassword="");

	set(domainName="http://dev.openmonitor.com");

</cfscript>
