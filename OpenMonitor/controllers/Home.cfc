component extends="Controller" output="false" {

	function init() {
		super.init();
		provides('html,json');

	}

	function index() {

		usesLayout("/layouts/main");

		sonorities = model("sensorvalue").findAll(where="sensortypeid = 2");
		temperature = model("sensorvalue").findOne(where="sensortypeid=4", order="createddate DESC");
		air = model("sensorvalue").findOne(where="sensortypeid=1", order="createddate DESC");
		humidity = model("sensorvalue").findOne(where="sensortypeid=6", order="createddate DESC");
		pressure = model("sensorvalue").findOne(where="sensortypeid=5", order="createddate DESC");
		light = model("sensorvalue").findOne(where="sensortypeid=3", order="createddate DESC");

		if (isBoolean(light)) {
			light = {};
			light.sensorvalue = 0;
		}
		if (isBoolean(pressure)) {
			pressure = {};
			pressure.sensorvalue = 0;
		}
		if (isBoolean(humidity)) {
			humidity = {};
			humidity.sensorvalue = 0;
		}
		if (isBoolean(air)) {
			air = {};
			air.sensorvalue = 0;
		}
		if (isBoolean(temperature)) {
			temperature = {};
			temperature.sensorvalue = 0;
		}

		arrayChart = arrayNew(2);
		for (i = 0; i <= 59; i++) {
			 arrayChart[i+1][1] = dateAdd('s', i - 59, now());
			 arrayChart[i+1][2] = 0;
			 for (row in sonorities) {
			 	//row.createdat = dateAdd('h', 2, row.createddate);
				if (dateTimeFormat(row.createddate, "medium") EQ dateTimeFormat(arrayChart[i+1][1], "medium")) {
					arrayChart[i+1][2] = row.sensorvalue;
				}
			 }
		}

	}



	//////////////////////////////////
	////////////SONORITY/////////////
	////////////////////////////////

	function sonority() {

		usesLayout("/layouts/main");



		sonority = model("sensorvalue").new();
		sonority.sensortypeid = 2;
		sonority.sensorvalue = randRange(1, 50);
		sonority.createddate = now();
		sonority.save();


		sonorities = model("sensorvalue").findAll(where="sensortypeid = 2");

		arrayChart = arrayNew(2);
		for (i = 0; i <= 59; i++) {
			 arrayChart[i+1][1] = dateAdd('n', i - 59, now());
			 arrayChart[i+1][2] = 0;
			 for (row in sonorities) {
			 	//row.createddate = dateAdd('h', 2, row.createddate);
				if (dateTimeFormat(row.createddate, "short") EQ dateTimeFormat(arrayChart[i+1][1], "short")) {
					arrayChart[i+1][2] = row.sensorvalue;
				}
			 }
		}


	}

	function updateSonority() {

		provides('html,json');

		sonorities = model("sensorvalue").findAll(where="sensortypeid = 2");

		var tmp = dateDiff("s", session.task, now());

		if (tmp GTE 120){
			model("sensorvalue").deleteAll(where="sensortypeid=2 AND createddate < DATE_ADD(NOW(),INTERVAL -2 MINUTE)");
			session.task = now();
		}

		arrayChart = arrayNew(2);
		for (i = 0; i <= 65; i++) {
			 arrayChart[i+1][1] = dateAdd('s', i - 65, now());
			 arrayChart[i+1][2] = 0;
			 for (row in sonorities) {
			 	//row.createddate = dateAdd('h', 2, row.createddate);
				if (dateTimeFormat(row.createddate, "medium") EQ dateTimeFormat(arrayChart[i+1][1], "medium")) {
					arrayChart[i+1][2] = row.sensorvalue;
					 arrayChart[i+1][1] = i+1;
				}
			 }

		}
		renderWith(arrayChart);
	}

	function updateTemperature() {

		provides('html,json');

		temperature = model("sensorvalue").findOne(where="sensortypeid=4", order="createddate DESC");

		renderWith(temperature.sensorvalue);
	}

	function updateAir() {

		provides('html,json');

		air = model("sensorvalue").findOne(where="sensortypeid=1", order="createddate DESC");

		renderWith(air.sensorvalue);
	}

	function updateHumidity() {

		provides('html,json');

		humidity = model("sensorvalue").findOne(where="sensortypeid=6", order="createddate DESC");

		renderWith(humidity.sensorvalue);
	}

	function updatePressure() {

		provides('html,json');

		pressure = model("sensorvalue").findOne(where="sensortypeid=5", order="createddate DESC");

		renderWith(pressure.sensorvalue);
	}

	function updateLight() {

		provides('html,json');

		light = model("sensorvalue").findOne(where="sensortypeid=3", order="createddate DESC");

		renderWith(light.sensorvalue);
	}

	function task() {
		provides('html,json');
		test = model("sensorvalue").deleteAll(where="sensortypeid=2");
		renderWith(test);
	}

	//////////////////////////////////
	////////////AIR//////////////////
	////////////////////////////////

	//////////////////////////////////
	////////////LIGHT////////////////
	////////////////////////////////

	//////////////////////////////////
	////////////ATMOSPHERE///////////
	////////////////////////////////



}