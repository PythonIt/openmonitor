<cfcomponent extends="Model" output="false">

	<cffunction name='init'>
		<cfset super.init()>

		<!-- associations -->
		<cfset belongsTo(name='sensortype', foreignKey='sensortypeid')>

	</cffunction>

</cfcomponent>