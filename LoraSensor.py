import serial
import time
import mysql.connector
import traceback
import sys

time.sleep(60)
port = 'COM4'
if len(sys.argv) > 1 :
    port =  sys.argv[1]

ser = serial.Serial(port, 57600, timeout=60)
cnx = mysql.connector.connect(user='root', password='', host='localhost', database='hackathonmic')
cursor = cnx.cursor()
count = 90
while 1:
    try:
        lineReceived = ser.readline()
        lineReceived = lineReceived.replace('\r\n' , '' )
        split = lineReceived.split(';')
        if len(split) == 7 and 'Pressure' in lineReceived:
            air = split[0].split(':')[1]
            sound = split[1].split(':')[1]
            light = split[2].split(':')[1]
            temperature = split[3].split(':')[1]
            hpa = split[4].split(':')[1]
            altitude = split[5].split(':')[1]
            humidite = split[6].split(':')[1]
            insert = "INSERT INTO sensorvalues(SensorTypeId, SensorValue) VALUES ({0}, {1})".format(2, sound)
            cursor.execute(insert)
	    if count == 90: 
		insert = "INSERT INTO sensorvalues(SensorTypeId, SensorValue) VALUES ({0}, {1})".format(1, air)
		cursor.execute(insert)
		insert = "INSERT INTO sensorvalues(SensorTypeId, SensorValue) VALUES ({0}, {1})".format(3, light)
		cursor.execute(insert)
		insert = "INSERT INTO sensorvalues(SensorTypeId, SensorValue) VALUES ({0}, {1})".format(4, temperature)
		cursor.execute(insert)
		insert = "INSERT INTO sensorvalues(SensorTypeId, SensorValue) VALUES ({0}, {1})".format(5, hpa)
		cursor.execute(insert)
		insert = "INSERT INTO sensorvalues(SensorTypeId, SensorValue) VALUES ({0}, {1})".format(6, humidite)
		cursor.execute(insert)
		count = 1
	    count += 1
            cnx.commit()
    except :
        traceback.print_exc()
    time.sleep(1)
cursor.close()
cnx.close()
