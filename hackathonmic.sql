-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Client :  127.0.0.1
-- Généré le :  Sam 29 Avril 2017 à 14:10
-- Version du serveur :  5.7.14
-- Version de PHP :  5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `hackathonmic`
--

-- --------------------------------------------------------

--
-- Structure de la table `sensortypes`
--

CREATE TABLE `sensortypes` (
  `Id` int(11) NOT NULL,
  `Name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `sensorvalues`
--

CREATE TABLE `sensorvalues` (
  `Id` int(11) NOT NULL,
  `SensorTypeId` int(11) NOT NULL,
  `SensorValue` int(11) NOT NULL,
  `CreatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Index pour les tables exportées
--

--
-- Index pour la table `sensortypes`
--
ALTER TABLE `sensortypes`
  ADD PRIMARY KEY (`Id`);

--
-- Index pour la table `sensorvalues`
--
ALTER TABLE `sensorvalues`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `SensorTypeId` (`SensorTypeId`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `sensorvalues`
--
ALTER TABLE `sensorvalues`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6209;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `sensorvalues`
--
ALTER TABLE `sensorvalues`
  ADD CONSTRAINT `sensorvalues_ibfk_1` FOREIGN KEY (`SensorTypeId`) REFERENCES `sensortypes` (`Id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
