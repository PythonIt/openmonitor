/*
   Copyright 2015-2016 AllThingsTalk

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*/  

/****
 *  AllThingsTalk Developer Cloud IoT experiment for LoRa
 *  Version 1.0 dd 09/11/2015
 *  Original author: Jan Bogaerts 2015
 *
 *  This sketch is part of the AllThingsTalk LoRa rapid development kit
 *  -> http://www.allthingstalk.com/lora-rapid-development-kit
 *
 *  This example sketch is based on the Proxilmus IoT network in Belgium
 *  The sketch and libs included support the
 *  - MicroChip RN2483 LoRa module
 *  - Embit LoRa modem EMB-LR1272
 *  
 *  For more information, please check our documentation
 *  -> http://allthingstalk.com/docs/tutorials/lora/setup
 *
 * Explanation:
 * 
 * We will measure our environment using 6 sensors. Approximately, every 2 minutes, all values 
 * will be read and sent to the AllthingsTalk Developer Cloud.
 * 
 **/

#include <Wire.h>
#include <AirQuality2.h>
#include <ATT_LoRa_IOT.h>
#include "keys.h"
#include <MicrochipLoRaModem.h>
#include <SPI.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BME280.h>

#define BME_SCK 13
#define BME_MISO 12
#define BME_MOSI 11
#define BME_CS 10

#define SEALEVELPRESSURE_HPA (1013.25)

Adafruit_BME280 bme; // I2C

#define SERIAL_BAUD 57600
#define AirQualityPin A0
#define LightSensorPin A2
#define SoundSensorPin A4

#define SEND_EVERY 1000


MicrochipLoRaModem Modem(&Serial1, &Serial);
ATTDevice Device(&Modem, &Serial);
AirQuality2 airqualitysensor;

float soundValue;
float lightValue;
float temp;
float hum;
float pres;
short airValue;


void setup() 
{
  pinMode(GROVEPWR, OUTPUT);                                    // turn on the power for the secondary row of grove connectors.
  digitalWrite(GROVEPWR, HIGH);
  while((!Serial) && (millis()) < 2000){}						//wait until serial bus is available, so we get the correct logging on screen. If no serial, then blocks for 2 seconds before run
  Serial.begin(SERIAL_BAUD);
  Serial1.begin(Modem.getDefaultBaudRate());                    // init the baud rate of the serial connection so that it's ok for the modem
  //Serial.println(F("BME280 test"));

  if (!bme.begin()) {
    Serial.println("Could not find a valid BME280 sensor, check wiring!");
    while (1);
  }
  initSensors();
}

void loop() 
{
  ReadSensors();

  //Make the string to be splitted by a ;
  Serial.print("airValue:");
  Serial.print(airValue);
  Serial.print(";soundValue:");
  Serial.print(soundValue);
  Serial.print(";lightValue:");
  Serial.print(lightValue);
  Serial.print(";");
  Serial.print("Temperature:");
  Serial.print(bme.readTemperature());
  Serial.print(";");

  Serial.print("Pressure:");

  Serial.print(bme.readPressure() / 100.0F);
  Serial.print(";");
  Serial.print("Approx. Altitude:");
  Serial.print(bme.readAltitude(SEALEVELPRESSURE_HPA));
  Serial.print(";");

  Serial.print("Humidity:");
  Serial.print(bme.readHumidity());
  Serial.println("");
  delay(SEND_EVERY);
}

void initSensors()
{
    Serial.println("Initializing sensors, this can take a few seconds...");
    pinMode(SoundSensorPin,INPUT);
    airqualitysensor.init(AirQualityPin);
    Serial.println("Done");
}

void ReadSensors()
{
    soundValue = analogRead(SoundSensorPin);
    lightValue = analogRead(LightSensorPin);
    lightValue = lightValue * 3.3 / 1023;         // convert to lux, this is based on the voltage that the sensor receives
    lightValue = pow(10, lightValue);    
    airValue = airqualitysensor.getRawData();
}

void SendSensorValues()
{
    Serial.println("Start uploading data to the ATT cloud Platform");
    Serial.println("----------------------------------------------");
    Serial.println("Sending sound value... ");
    Device.Send(soundValue, LOUDNESS_SENSOR);
    Serial.println("Sending light value... "); 
    Device.Send(lightValue, LIGHT_SENSOR);
    Serial.println("Sending air quality value... ");  
    Device.Send(airValue, AIR_QUALITY_SENSOR);
}

void DisplaySensorValues()
{
    Serial.print("Sound level: ");
    Serial.print(soundValue);
	  Serial.println(" Analog (0-1023)"); 
    Serial.print("Air quality: ");
    Serial.print(airValue);
	  Serial.println(" Analog (0-1023)");
}


void serialEvent1()
{
    Device.Process();                           //for future use of actuators
}


